<?php
require_once __DIR__ . '/lib/model/abstractShip.php';
require_once __DIR__ . '/lib/model/ship.php';
require_once __DIR__ . '/lib/model/battleResult.php';
require_once __DIR__ . '/lib/model/rebelShip.php';
require_once __DIR__ . '/lib/service/battleManager.php';
require_once __DIR__ . '/lib/service/shipLoader.php';
require_once __DIR__ . '/lib/service/container.php';

$configuration = array(
    'db_dsn' => 'mysql:host=localhost;dbname=oo_battle',
    'db_user' => 'root',
    'db_pass' => null
);