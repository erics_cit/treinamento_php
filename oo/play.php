<?php
require __DIR__.'/lib/ship.php';

$myShip = new ship();
$myShip->name = 'Perola negra';
echo 'The name of the ship is ' . $myShip->name;
echo '</br>';

$myShip->weaponPower = 100;
$myShip->jedi = 6;
$myShip->factor = 74;
$myShip->strength = 33;

echo $myShip->getNameAndSpecs(true);
echo '</br>';
echo $myShip->getNameAndSpecs(false);