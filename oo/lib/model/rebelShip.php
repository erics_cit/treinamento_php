<?php
class rebelShip extends abstractShip{
    public function getType(){
        return 'Rebel';
    }

    public function getNameAndSpecs($useShortFormat = true){        
        return parent::getNameAndSpecs($useShortFormat) . ' (Rebel)';
    }    
}
?>
