<?php
class abstractShip{
      public function __construct($name){
        $this->name = $name;
    }

    protected $id;
    protected $name;
    protected $weaponPower; 
    protected $jediFactor; 
    protected $strength;

    private $longFormat = 'The name of the ship is %01s, weapon_power=%02d, jedi factor=%03d, strength=%04d';  
    private $shortFormat = 'Ship is %01s w:%02d/j:%03d/s:%04d';

    public function getNameAndSpecs($useShortFormat = true){        
        return sprintf(
            ($useShortFormat) ? $this->shortFormat : $this->longFormat,
            $this->name,
            $this->weaponPower,
            $this->jediFactor,
            $this->strength    
        );
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getId(){
        return $this->id;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getName(){
        return $this->name;
    }

    public function setWeaponPower($weaponPower){
        $this->weaponPower = $weaponPower;
    }

    public function getWeaponPower(){
        return $this->weaponPower;
    }

    public function setJediFactor($jediFactor){
        $this->jediFactor = $jediFactor;
    }

    public function getJediFactor(){
        return $this->jediFactor;
    }

    public function setStrength($strength){
        $this->strength = $strength;
    }

    public function getStrength(){
        return $this->strength;
    }

    public function getType(){
        return 'Empire';
    }
}
?>