<?php
class battleResult{

    private $winningShip;
    private $losingShip;
    private $usedJediPowers;

    public function __construct($winningShip, $losingShip, $usedJediPowers){
        $this->winningShip = $winningShip;
        $this->losingShip = $losingShip;
        $this->usedJediPowers = $usedJediPowers;
    }

    public function getWinningShip(){
        return $this->winningShip;
    }

    public function getLosingShip(){
        return $this->losingShip;
    }

    public function wereJediPowersUsed(){
        return $this->usedJediPowers;
    }
}
?>
