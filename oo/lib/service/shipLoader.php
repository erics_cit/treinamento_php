<?php

class shipLoader
{
    private $pdo;

    public function __construct($pdo){
        $this->pdo = $pdo;
    }

    public function getShips()
    {
        $ships = array();
        
        foreach ($this->getQueryAllResult() as $shipResult) {
            $ships[] = $this->createShipByRecord($shipResult);    
        };

        return $ships;
    }

    public function getShip($id){
        $shipResult = $this->getQueryOneResult($id);
        if(!$shipResult){
            return null;
        }

        return $this->createShipByRecord($shipResult);
    }

    private function getQueryAllResult(){
        $statement = $this->pdo->prepare('SELECT * FROM oo_battle.ship');
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    private function getQueryOneResult($id){
        $statement = $this->pdo->prepare('SELECT * FROM oo_battle.ship where id = :id');
        $statement->bindParam('id', $id);
        $statement->execute();
        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    private function createShipByRecord($shipRecord){
        $ship = $shipRecord['team'] == 'rebel' ? new rebelShip($shipRecord['name']) : new ship($shipRecord['name']);
        $ship->setWeaponPower($shipRecord['weapon_power']);
        $ship->setJediFactor($shipRecord['jedi_factor']);
        $ship->setStrength($shipRecord['strength']);
        $ship->setId($shipRecord['id']);

        return $ship;  
    }
}