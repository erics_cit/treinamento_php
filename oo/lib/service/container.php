<?php
class container{
    private $pdo;
    private $shipLoader;
    private $battleManager;
    private $configuration;

    public function __construct($configuration){
        $this->configuration = $configuration;
    }

    public function getShipLoader(){
        if($this->shipLoader == null){
            $this->shipLoader = new shipLoader($this->getPDO());
        }

        return $this->shipLoader;
    }

    public function getBattleManager(){
        if($this->battleManager == null){
            $this->battleManager = new battleManager();
        }
        return $this->battleManager;
    }

    private function getPDO(){
        if($this->pdo == null){
            $this->pdo = new PDO($this->configuration['db_dsn'],
                                 $this->configuration['db_user'], 
                                 $this->configuration['db_pass']);

            
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        return $this->pdo;
    }
}
?>