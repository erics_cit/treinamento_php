<?php
function getAmountPets(){
    $result = get_pdo()->query('select count(id) as amount from air_pup.pet');
    return $result->Fetch()['amount'];
    
    return count(json_decode(file_get_contents('resources/pets.json'), true));
}

function getPets(){
    $result = get_pdo()->query('SELECT * FROM air_pup.pet;');
    return $result->FetchAll();
}

function get_pet($pet_id){
    $query = 'SELECT * FROM air_pup.pet where id = :idVal';
    $stmt = get_pdo()->prepare($query);
    $stmt->bindParam('idVal', $pet_id);
    $stmt->execute();
    return $stmt->Fetch();
}

function getPetsFromRequest($post){
    extract ($post, EXTR_PREFIX_SAME, "wddx");
    
    return array(
    'name' => $name,
    'breed' => $breed,
    'weight' => $weight,
    'bio' => $bio,
    'filename' => basename($_FILES["image"]["name"]),
    'age' => $age,
    );
}

function get_pdo(){
    $config = require('config.php');
    $pdo = new PDO($config['database_dsn'],
    $config['user'],
    $config['password']);
    return $pdo;
}

function savePets(){
    $pets = getPets();
    $pets[] = getPetsFromRequest($_POST);
    file_put_contents('resources/pets.json', json_encode($pets, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK ));
}

function saveImage(){
    $target_dir = "images//";
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
}
?>