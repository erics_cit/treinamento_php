<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">

    <title>AirPupnMeow.com: All the love, none of the crap!</title>

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <?php
        require('layout/header.php');
    ?>

    <?php
        #$pets = json_decode(file_get_contents('resources/pets.json'), true);

        require('lib/functions.php');
        $pets = getPets();
    ?>

    <div class="jumbotron">
        <div class="container">
            <h1> 
                <?php 
                    echo strtoupper("Hello, world!");
                ?>
            </h1>

            <p>This is a template for a simple marketing or informational website. It includes a large callout called the
                hero unit and three supporting pieces of content. Use it as a starting point to create something more
                unique.</p>

            <p><a class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
        </div>
    </div>

    <div class="container">
            <h2>
                <?php
                    echo 'Showing ' . count($pets) . ' animals!';
                ?>
            </h2>
            <div class="row">
                <?php foreach ($pets as $pet) { ?>
                        <div class="col-lg-4 pet-list-item">
                            <h2>
                                <a href="/pet.php?pid=<?php echo $pet['id']?>"> 
                                    <?php echo $pet['name']
                                    ?>
                                </a>
                            </h2>
                            <?php if (array_key_exists('filename', $pet) && $pet['filename'] != '') { ?>
                                <img src=<?php echo 'images/' . $pet['filename']?> />                            
                            <?php } else { ?>
                                <p>Image is not available</p>
                            <?php } ?>

                            <p>
                                <?php echo $pet['bio'] 
                                ?>
                            </p>    
                            <p>
                                <?php
                                    echo $pet['breed'] . '    ' . $pet['weight'] . '  ' . ($pet['weight'] > 8 ? 'I need some exercise' : 'I’m slim!');
                                    #echo $pet['breed'] . '    ' . $pet['weight'];
                                ?>
                            </p>
                        </div>
                <?php } ?>
            </div>
    </div>


    <body>
    <?php 
    $arrayHeading = array('Block1', 'Block2', 'Block3');
    ?>

    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2>
                <?php 
echo $arrayHeading[0];
?>
                </h2>

                <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
                    condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
                    euismod. Donec sed odio dui. </p>

                <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                                <h2>
                <?php 
echo $arrayHeading[1];
?>
                </h2>

                <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
                    condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
                    euismod. Donec sed odio dui. </p>

                <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                                <h2>
                <?php 
echo $arrayHeading[2];
?>
                </h2>

                <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula
                    porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                    fermentum massa justo sit amet risus.</p>

                <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
            </div>
        </div>
    </div>

    <?php
        require('layout/footer.php');
    ?>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
