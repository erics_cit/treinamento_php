<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="favicon.ico">

        <title>AirPupnMeow.com: All the love, none of the crap!</title>

        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <?php
            require('layout/header.php');
            $pet_id = $_GET['pid'];
            if(!$pet_id){
                header("location:/404.php");
            }

            require('./lib/functions.php');

            $pet = get_pet($pet_id);
            if(!$pet){
                header("location:/404.php");
            }
        ?>

         <h1>
            <?php
                echo 'Showing details of ' . $pet['name'];
            ?>
        </h1>

        <div class="container">
            <div class="row">
                <div class="col-lg-3 pet-list-item">
                    <?php if (array_key_exists('filename', $pet) && $pet['filename'] != '') { ?>
                        <img src=<?php echo 'images/' . $pet[ 'filename']?> class="pull-left img-round" />
                    <?php } else { ?>
                        <p>Image is not available</p>
                    <?php } ?>
                </div>
                <div class="col-lg-6">
                    <p>
                        <?php echo $pet['bio']
                        ?>
                    </p>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Breed</th>
                                <td>
                                    <?php echo $pet['breed']
                                    ?>
                                </td>
                                <th>Weight</th>
                                <td>
                                    <?php echo $pet['weight']
                                    ?>                                    
                                </td>
                                <th>Age</th>
                                <td>
                                    <?php echo $pet['age']
                                    ?>
                                </td>
                            </tr>
                        </thead>
                    </table>
                    <p>
                        <strong>
                            <?php
                                echo ($pet['weight'] > 8 ? 'I need some exercise' : 'I’m slim!');
                            ?>
                        </strong>
                    </p>
                </div>
            </div>
        </div>
        <?php
            require('layout/footer.php');
        ?>

        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>