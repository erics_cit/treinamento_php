<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">

    <title>AirPupnMeow.com: All the love, none of the crap!</title>

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php
        require('layout/header.php');
        require('lib/functions.php');

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            saveImage();       
            savePets();
            header("location:/"); 
            die;
        }
    ?>
    <div class="container">
        <div class='row'>
            <h2>Add a new pet </h3>
            <form action="/add_pet.php" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="pet-name" class="control-label">Pet Name</label>
                        <input type="text" name="name" id="pet-name" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="pet-breed" class="control-label">Breed</label>
                        <input type="text" name="breed" id="pet-breed" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="pet-weight" class="control-label">Weight (lbs)</label>
                        <input type="number" name="weight" id="pet-weight" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="pet-age" class="control-label">Age</label>
                        <input type="number" name="age" id="pet-age" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="pet-bio" class="control-label">Pet Bio</label>
                        <textarea name="bio" id="pet-bio" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="pet-image" class="control-label">Image</label>
                        <input type="file" name="image" id="pet-image" class="form-control" />
                    </div>

                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-heart"></span> Add</button>
                </form>
            </div>
        </div>
    </div>

    <?php
        require('layout/footer.php');
    ?>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
